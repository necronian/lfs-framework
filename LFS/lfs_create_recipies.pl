use strict;
use warnings;
use File::Slurp 'slurp';
use Mojo::DOM;

my $dom = Mojo::DOM->new->parse(scalar slurp $ARGV[0]);

my $x = 1;

# These sections are the user input minus the package build instructions? TODO: Maybe?
for my $d ($dom->find('div.sect1,div.sect2')->each) {
    
    my $contents = "";
    
    for my $s ($d->find('pre.userinput')->each) {
        $contents .= $s->all_text;
    }
    
    if ($contents ne "") {
        
        # Try to get the title of the section
        my $name = $d->at('h2.title') // $d->at('h3.sect2');
                
        if ($name)  {
            $name = $name->all_text;
            # Strip out unnecessary characters for the filenames. 
            $name =~ tr/a-zA-Z//dc;
        } else {
            $name = "";
        }
        
        # Number each section sequentially 
        my $filename = sprintf("%02d-%s.sh", $x,$name);
        
        open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";
        
        # Write the file contents
        print $fh $contents;
        
        close $fh;
        $x += 1;
    }
}
