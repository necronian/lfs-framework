#!/bin/bash

. ./lfs_variables.sh
. ./package_variables.sh
. ./lfs_functions.sh

RECIPIES=toolchain_recipies

for file in `ls $RECIPIES`
do
    . $RECIPIES/$file
done
