#!/bin/bash

. ./lfs_variables.sh
. ./package_variables.sh
. ./lfs_functions.sh


chown -R root:root $LFS/tools
chown -R root:root $LFS/sources

mkdir -v $LFS/{dev,proc,sys}
mknod -m 600 $LFS/dev/console c 5 1
mknod -m 666 $LFS/dev/null c 1 3
mount -v --bind /dev $LFS/dev
mount -vt devpts devpts $LFS/dev/pts
mount -vt tmpfs shm $LFS/dev/shm
mount -vt proc proc $LFS/proc
mount -vt sysfs sysfs $LFS/sys

cp lfs_chroot_setup.sh $LFS/
