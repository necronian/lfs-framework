#!/bin/bash

NUM=printf '%d' $(( $(ls | tail -n 1 | sed -e 's/_.*//') + 1 ))
cat sample | sed "s/FUNCTIONNAME/$1/" > "${NUM}${1}.sh"
$EDITOR "${NUM}${1}.sh"
