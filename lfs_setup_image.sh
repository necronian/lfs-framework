#!/bin/bash

#************************************************#
#               lfs_setup_image.sh               #
#                Jeffrey Stoffers                #
#                October 31, 2011                #
#                                                #
#    Set up an environment in which to build     #
#          a linux from scratch system           #
#************************************************#

. ./lfs_variables.sh

IMAGE_SIZE=10000000
IMAGE_NAME="../lfsIMAGE"
SOURCES_URL="http://www.linuxfromscratch.org/lfs/view/stable/wget-list"
MD5SUMS_URL="http://www.linuxfromscratch.org/lfs/view/stable/md5sums"

# --------------------------------------------------------- #
# create_image ()                                           #
# Use dd to create an empty file and then format it as ext3 #
# Parameter:                                                #
# Returns:                                                  #
# --------------------------------------------------------- #
create_image () {
    dd if=/dev/zero of=${IMAGE_NAME} bs=1024 count=${IMAGE_SIZE}
    yes | mkfs -v -t ext4 ${IMAGE_NAME}
}

# --------------------------------------------------------- #
# mount_image ()                                            #
# Mount the image file using a loopback                     #
# Parameter:                                                #
# Returns:                                                  #
# --------------------------------------------------------- #
mount_image () {
    mkdir -pv $LFS
    mount -v -t ext4 -o loop ${IMAGE_NAME} $LFS
}

# --------------------------------------------------------- #
# create_package_variables ()                               #
# Create the package_variables.sh file                      #
# Parameter:                                                #
# Returns:                                                  #
# --------------------------------------------------------- #
create_package_variables () {
    wget ${SOURCES_URL}
    cat wget-list | sed -e 's/.*\///' | grep -v .patch | sed -e 's/\(.*\)/\1"/' | paste -d\" package_definitions - > package_variables.sh
}

# --------------------------------------------------------- #
# get_sources ()                                            #
# Download the lfs sources into the mounted image file      #
# Parameter:                                                #
# Returns:                                                  #
# --------------------------------------------------------- #
get_sources () {
    mkdir -v $LFS/sources
    chmod -v a+wt $LFS/sources

    create_package_variables
    wget -c -i wget-list -P $LFS/sources

    wget ${MD5SUMS_URL} -P $LFS/sources
    pushd  $LFS/sources
    md5sum -c md5sums
    popd
}

# --------------------------------------------------------- #
# setup_host_system ()                                      #
# Do the general bookkeeping for building the lfs toolchain #
# Parameter:                                                #
# Returns:                                                  #
# --------------------------------------------------------- #
setup_host_system () {
    mkdir -v $LFS/tools
    ln -sv $LFS/tools /

    groupadd lfs
    useradd -s /bin/bash -g lfs -m -k /dev/null lfs
    chown -v lfs $LFS/tools
    chown -v lfs $LFS/sources

    rm -rvf ~lfs/lfs-framework

    cp -rvf ../lfs-framework ~lfs
    mkdir ~lfs/lfs-framework/${LOGDIR}
    chown -R lfs:lfs ~lfs/lfs-framework

    cp configs/bash_profile ~lfs/.bash_profile
    cp configs/bashrc ~lfs/.bashrc

    chown lfs:lfs ~lfs/.bashrc ~lfs/.bash_profile
}

#su -l -c "cd ~/lfs; sh lfs_install_toolchain.sh" lfs

usage()
{
cat <<EOF
usage: $0 options

This script is for creating a lfs image and preparing it for installing a lfs system.

OPTIONS:
   -h      Show this message
   -i      Give image file a custom name
   -c      Don't run the image creation stage
   -m      Don't run the image mounting stage
   -g      Don't run the get sources stage
   -s      Don't run the setup host stage
   -p      Recreate the package_variables.sh
EOF
}

CI=true
MI=true
GS=true
SH=true
CP=false

while getopts “hcmgspi:” OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
	 i)
	     IMAGE_NAME=${OPTARG}
	     ;;
	 c)
	     CI=false
	     ;;
	 m)
	     MI=false
	     ;;
	 g)
	     GS=false
	     ;;
	 s)
	     SH=false
	     ;;
	 p)
	     CP=true
	     ;;
     esac
done

if $CI
then
    create_image
fi

if $MI
then
    mount_image
fi

if $GS
then
    get_sources
fi

if $CP
then
    create_package_variables
fi

if $SH
then
    setup_host_system
fi
