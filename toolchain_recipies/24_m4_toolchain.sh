m4_toolchain_install_commands () {
    cd m4-1.4.17
    ./configure --prefix=/tools
    make
    make install
}

m4_toolchain_remove_commands () {
    \;
}

m4_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

m4_toolchain
