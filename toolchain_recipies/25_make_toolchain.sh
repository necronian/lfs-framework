make_toolchain_install_commands () {
    cd make-4.1
    ./configure --prefix=/tools --without-guile
    make
    make install
}

make_toolchain_remove_commands () {
    \;
}

make_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

make_toolchain
