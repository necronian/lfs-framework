bash_toolchain_install_commands () {
    cd bash-4.3.30

    ./configure --prefix=/tools --without-bash-malloc

    make
    make install

    ln -sv bash /tools/bin/sh
}

bash_toolchain_remove_commands () {
    \;
}

bash_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

bash_toolchain
