gawk_toolchain_install_commands () {
    cd gawk-4.1.3
    ./configure --prefix=/tools
    make
    make install
}

gawk_toolchain_remove_commands () {
    \;
}

gawk_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

gawk_toolchain
