binutils_toolchain_pass_2_install_commands () {
    cd binutils-2.25.1

    mkdir -v ../binutils-build
    cd ../binutils-build

    CC=$LFS_TGT-gcc              \
    AR=$LFS_TGT-ar               \
    RANLIB=$LFS_TGT-ranlib       \
    ../binutils-2.25.1/configure \
      --prefix=/tools            \
      --disable-nls              \
      --disable-werror           \
      --with-lib-path=/tools/lib \
      --with-sysroot

    make
    make install

    make -C ld clean
    make -C ld LIB_PATH=/usr/lib:/lib
    cp -v ld/ld-new /tools/bin
}

binutils_toolchain_pass_2_remove_commands () {
    rm -rvf binutils-build
}

binutils_toolchain_pass_2 () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

binutils_toolchain_pass_2
