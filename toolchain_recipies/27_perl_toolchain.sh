perl_toolchain_install_commands () {
    cd perl-5.22.0
    sh Configure -des -Dprefix=/tools -Dlibs=-lm
    make
    cp -v perl cpan/podlators/pod2man /tools/bin
    mkdir -pv /tools/lib/perl5/5.22.0
    cp -Rv lib/* /tools/lib/perl5/5.22.0
}

perl_toolchain_remove_commands () {
    \;
}

perl_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

perl_toolchain
