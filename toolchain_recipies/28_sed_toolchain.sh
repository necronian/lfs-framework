sed_toolchain_install_commands () {
    cd sed-4.2.2
    ./configure --prefix=/tools
    make
    make install
}

sed_toolchain_remove_commands () {
    \;
}

sed_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

sed_toolchain
