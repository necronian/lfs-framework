file_toolchain_install_commands () {
    cd file-5.24
    ./configure --prefix=/tools
    make
    make install
}

file_toolchain_remove_commands () {
    \;
}

file_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

file_toolchain
