check_toolchain_install_commands () {
    cd check-0.10.0
    PKG_CONFIG= ./configure --prefix=/tools
    make
    make install
}

check_toolchain_remove_commands () {
    \;
}

check_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

check_toolchain
