expect_toolchain_install_commands () {
    cd expect5.45

    cp -v configure{,.orig}
    sed 's:/usr/local/bin:/bin:' configure.orig > configure

    ./configure --prefix=/tools       \
                --with-tcl=/tools/lib \
                --with-tclinclude=/tools/include

    make

    make SCRIPTS="" install

}

expect_toolchain_remove_commands () {
\;
}

expect_toolchain () {
	install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

expect_toolchain
