libstdc_toolchain_install_commands () {
    cd gcc-5.2.0

    mkdir -pv ../gcc-build
    cd ../gcc-build

    ../gcc-5.2.0/libstdc++-v3/configure \
        --host=$LFS_TGT                 \
        --prefix=/tools                 \
        --disable-multilib              \
        --disable-nls                   \
        --disable-libstdcxx-threads     \
        --disable-libstdcxx-pch         \
        --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/5.2.0

    make

    make install
}

libstdc_toolchain_remove_commands () {
    rm -rvf gcc-build
}

libstdc_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

libstdc_toolchain
