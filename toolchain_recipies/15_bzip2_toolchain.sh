bzip2_toolchain_install_commands () {
    cd bzip2-1.0.6
    make
    make PREFIX=/tools install
}

bzip2_toolchain_remove_commands () {
    \;
}

bzip2_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

bzip2_toolchain
