util-linux_toolchain_install_commands () {
    cd util-linux-2.27
    ./configure --prefix=/tools                \
                --without-python               \
                --disable-makeinstall-chown    \
                --without-systemdsystemunitdir \
                PKG_CONFIG=""
    make
    make install
}

util-linux_toolchain_remove_commands () {
    \;
}

util-linux_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

util-linux_toolchain
