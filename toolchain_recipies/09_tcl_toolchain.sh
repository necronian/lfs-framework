tcl_toolchain_install_commands () {
    cd tcl8.6.4
    cd unix
    ./configure --prefix=/tools
    TZ=UTC make
    make install
    chmod -v u+w /tools/lib/libtcl8.6.so

    make install-private-headers
    ln -sv tclsh8.6 /tools/bin/tclsh
}

tcl_toolchain_remove_commands () {
    rm -rvf tcl8.6.4
}

tcl_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

tcl_toolchain
