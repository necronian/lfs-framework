xz_toolchain_install_commands () {
    cd xz-5.2.1
    ./configure --prefix=/tools
    make
    make install
}

xz_toolchain_remove_commands () {
    \;
}

xz_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

xz_toolchain
