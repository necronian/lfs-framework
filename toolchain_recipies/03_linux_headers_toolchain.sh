linux_headers_toolchain_install_commands () {
    cd linux-4.2
    
    make mrproper

    make INSTALL_HDR_PATH=dest headers_install
    cp -rv dest/include/* /tools/include
}

linux_headers_toolchain_remove_commands () {
    \;
}

linux_headers_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

linux_headers_toolchain
