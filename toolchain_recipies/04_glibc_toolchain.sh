glibc_toolchain_install_commands () {
    cd glibc-2.22

    patch -Np1 -i ../glibc-2.22-upstream_i386_fix-1.patch

    mkdir -v ../glibc-build
    cd ../glibc-build

    ../glibc-2.22/configure                           \
        --prefix=/tools                               \
        --host=$LFS_TGT                               \
        --build=$(../glibc-2.22/scripts/config.guess) \
        --disable-profile                             \
        --enable-kernel=2.6.32                        \
        --with-headers=/tools/include                 \
        libc_cv_forced_unwind=yes                     \
        libc_cv_ctors_header=yes                      \
        libc_cv_c_cleanup=yes

    make

    make install
}

glibc_toolchain_remove_commands () {
    rm -rvf glibc-build
}

glibc_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

glibc_toolchain
