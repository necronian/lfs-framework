dejagnu_toolchain_install_commands () {
    cd dejagnu-1.5.3
    ./configure --prefix=/tools
    make install
}

dejagnu_toolchain_remove_commands () {
    \;
}

dejagnu_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

dejagnu_toolchain
