patch_toolchain_install_commands () {
    cd patch-2.7.5
    ./configure --prefix=/tools
    make
    make install
}

patch_toolchain_remove_commands () {
    \;
}

patch_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

patch_toolchain
