tar_toolchain_install_commands () {
    cd tar-1.28
    ./configure --prefix=/tools
    make
    make install
}

tar_toolchain_remove_commands () {
    \;
}

tar_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

tar_toolchain
