gettext_toolchain_install_commands () {
    cd gettext-0.19.5.1
    cd gettext-tools
    EMACS="no" ./configure --prefix=/tools --disable-shared

    make -C gnulib-lib
    make -C intl pluralx.c
    make -C src msgfmt
    make -C src msgmerge
    make -C src xgettext
    
    cp -v src/{msgfmt,msgmerge,xgettext} /tools/bin
}

gettext_toolchain_remove_commands () {
    \;
}

gettext_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

gettext_toolchain
