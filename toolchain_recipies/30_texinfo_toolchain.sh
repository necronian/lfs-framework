texinfo_toolchain_install_commands () {
    cd texinfo-6
    ./configure --prefix=/tools
    make
    make install
}

texinfo_toolchain_remove_commands () {
    \;
}

texinfo_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

texinfo_toolchain
