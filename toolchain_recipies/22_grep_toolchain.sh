grep_toolchain_install_commands () {
    cd grep-2.21
    ./configure --prefix=/tools
    make
    make install
}

grep_toolchain_remove_commands () {
    \;
}

grep_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

grep_toolchain
