diffutils_toolchain_install_commands () {
    cd diffutils-3.3
    ./configure --prefix=/tools
    make
    make install
}

diffutils_toolchain_remove_commands () {
    \;
}

diffutils_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

diffutils_toolchain
