ncurses_toolchain_install_commands () {
    cd ncurses-6.0

    ./configure --prefix=/tools \
                --with-shared   \
                --without-debug \
                --without-ada   \
                --enable-widec  \
                --enable-overwrite

    make
    make install
}

ncurses_toolchain_remove_commands () {
    \;
}

ncurses_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

ncurses_toolchain
