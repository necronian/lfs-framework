findutils_toolchain_install_commands () {
    cd findutils-4.4.2
    ./configure --prefix=/tools
    make
    make install
}

findutils_toolchain_remove_commands () {
    \;
}

findutils_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

findutils_toolchain
