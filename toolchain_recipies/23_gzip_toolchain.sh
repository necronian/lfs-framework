gzip_toolchain_install_commands () {
    cd gzip-1.6
    ./configure --prefix=/tools
    make
    make install
}

gzip_toolchain_remove_commands () {
    \;
}

gzip_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

gzip_toolchain
