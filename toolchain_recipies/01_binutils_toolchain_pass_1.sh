binutils_toolchain_pass_1_install_commands () {
    cd binutils-2.25.1
    mkdir -v ../binutils-build
    cd ../binutils-build
    ../binutils-2.25.1/configure     \
        --prefix=/tools            \
        --with-sysroot=$LFS        \
        --with-lib-path=/tools/lib \
        --target=$LFS_TGT          \
        --disable-nls              \
        --disable-werror

    make

    case $(uname -m) in
        x86_64) mkdir -v /tools/lib && ln -sv lib /tools/lib64 ;;
    esac
    make install
}

binutils_toolchain_pass_1_remove_commands () {
    rm -rvf binutils-build
    rm -rvf binutils-2.25.1
}

binutils_toolchain_pass_1 () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

binutils_toolchain_pass_1
