coreutils_toolchain_install_commands () {
    cd coreutils-8.24
    ./configure --prefix=/tools --enable-install-program=hostname
    make
    make install
}

coreutils_toolchain_remove_commands () {
    \;
}

coreutils_toolchain () {
    install_package "${FUNCNAME%%_*}" "${FUNCNAME}"
}

coreutils_toolchain
