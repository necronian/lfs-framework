untar () {
	cd "${LFS_SOURCES}"
	tar -xvf "${1}"
	cd "${1%.tar.*}"
}

rmdir () {
	cd "${LFS_SOURCES}"
	rm -rvf "${1%.tar.*}"
}

install_package () {
	((time (( install_package_wrapped "${1}" "${2}" 3>&1 1>&2 2>&3 ) | tee "${LOGDIR}/${2}-error.log" ) 2>&1| tee "${LOGDIR}/${2}-all.log") 2> "${LOGDIR}/${2}-time.log")
	wait
}

install_package_wrapped () {
	local package_function_name=''"$1"'_PACKAGE'
	local package_name=${!package_function_name}
	local function_name=$2

	untar $package_name

	echo $function_name
	${function_name}_install_commands

	rmdir ${package_name}
	${function_name}_remove_commands
}
